﻿var categories = [
  '0-100', '100-200', '200-300', '300+'
];

$(function () {
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'dongdat',
        type: 'bar',
        options3d: {
            enabled: true,
            alpha: 0,
            beta: 30,
            depth: 50,
            viewDistance: 25
        },
    },
    title: {
        text: null,
        style: {
            color: '#000000',
            fontWeight: 'bold'
        }
    },
    subtitle: {
        text: null
    },
    xAxis: [{
		categories: categories,
		reversed: false,
		labels: {
		  step: 1
		}
	  }, { // mirror axis on right side
		opposite: true,
		reversed: false,
		categories: categories,
		linkedTo: 0,
		labels: {
		  step: 1
		}
	  }],
    yAxis: {
		title: {
		  text: null
		},
		labels: {
		  formatter: function () {
			return Math.abs(this.value);
		  }
		}
	  },
	legend: {
		fontSize:'5px'
    },
    colors: ['#38859B', '#FFA111', '#7CBBCF', '#B5D5E1', '#CDE2EB', '#D9EAF0', '#4F6096', '#6C6550', 'green', 'lightblue', 'lightgreen'],
	plotOptions: {
		series: {
		  stacking: 'normal'
		}
	  },
    series: [{
        name:'được kiểm soát',
        data: [-90, -50, -20, -10 ]
    },{
        name:'không được kiểm soát',
        data: [9, 5, 2, 0 ]
    }
    ],
    credits: {
        enabled: false
    },
    exporting: { enabled: false }
});

function showValues() {
    $('#alpha-value').html(chart.options.chart.options3d.alpha);
    $('#beta-value').html(chart.options.chart.options3d.beta);
    $('#depth-value').html(chart.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
    chart.options.chart.options3d[this.id] = parseFloat(this.value);
    showValues();
    chart.redraw(false);
});

showValues();
});